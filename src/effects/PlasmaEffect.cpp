#include <math.h>
#include "SDL.h"

#include "headers/PlasmaEffect.h"

void PlasmaEffect::init() {
    unsigned char *b1 = _buffer1;
    unsigned char *b2 = _buffer2;

    for (int j = 0; j < _buffer_h; ++j) {
        for (int i = 0; i < _buffer_w; ++i) {
            *b1 = (unsigned char) (128 + 63 * sin((double) hypot((int)_h - j, (int)_w - i) / 16)) + i;
            *b2 = (unsigned char) (32 + 63 * sin((double) i / (37 + 15 * cos((double) j / 74))) * cos((double) j / (31 + 11 * sin((double) i / 57)))) - j;
           
            *b1 = 64 + 63 * sin((double) hypot((double) _h - j, (double) _w - i ) / 16);
            b1++;
            b2++;
        }    
    }

    uint32_t *p = _palette;
    for (int k = 0; k < 256; k++) {
        *p = 0xFF000000 | (k % 128) << 16 | (k % 32) << 8 | k;
        // *p = 0xFF000000 | (k % 256) << 16 | (k % 64) << 8 | k * 4;
        // *p = 0xFF000000 | (2 * (k % 32)) << 16 | (k % 64) << 8 | k * 4;
        // *p = 0xFF000000 
        //     | (64 + (k % 16)) << 16 
        //     | (128 + (k % 8)) << 8 
        //     | 64 + (k % 128);
        p++;
    }
}

void PlasmaEffect::update(int deltaTime) {
    // rotate palette
    // _palette_index = (_palette_index + 1) % 256;

    // rotate offsets
    // _offset_x1 = (_buffer_w / 4) + (_w / 6) * sin(_alpha / 3);
    // _offset_y1 = (_buffer_h / 4) + (_h / 4) * cos(_alpha / 2);
    // _offset_x2 = (_buffer_w / 3) + (_w / 3) * sin(_alpha);
    // _offset_y2 = (_buffer_h / 4) + (_h / 4) * cos(_alpha / 6);

    _offset_x1 = (_buffer_w / 2) + (_w / 2) * sin(_alpha);
    _offset_y1 = (_buffer_h / 2) + (_h / 2) * cos(-_alpha / 2);
    _offset_x2 = (_buffer_w / 2) + (_w / 2) * sin(_alpha / 3);
    _offset_y2 = (_buffer_h / 2) + (_h / 2) * cos(-_alpha);

    // inc alpha
    _alpha += 0.017453292519943295; // PI / 180
    // if (_alpha > 6.283185307179586) {
    //     _alpha -= 6.283185307179586;
    // }
    uint32_t *p = _palette;
    for (int k = 0; k < 256; k++) {
        *p = (*p + 0x00010101) | 0xFF000000;
        p++;
    }
}

void PlasmaEffect::render(SDL_Surface* surface) {	
	Uint8 *dst = (Uint8 *)surface->pixels;
	int bpp = surface->format->BytesPerPixel;
    int pitch = surface->pitch;

    unsigned char *b1 = _buffer1 + _offset_y1 * _buffer_w + _offset_x1;
    unsigned char *b2 = _buffer2 + _offset_y2 * _buffer_w + _offset_x2;
    unsigned int color = 0;
    
    int mirror_line = _h / 2;
    int offset;

    SDL_LockSurface(surface);
    for (int j = 0; j < _h; ++j) {                
        for (int i = 0; i < _w; ++i) {
            if (j < mirror_line) {
                offset = (j * _buffer_w) + i;
            } else {
                offset = ((j - 2 * (j - mirror_line)) * _buffer_w) + i;                
            }

            b1 = _buffer1 + _offset_y1 * _buffer_w + _offset_x1 + offset;
            b2 = _buffer2 + _offset_y2 * _buffer_w + _offset_x2 + offset;
                        
            color = *(_palette + (*b1 + *b2 + _palette_index) % 256);
            if (j > mirror_line) {
                color = color & 0xFF0000FF;
            }

            *(Uint32 *)dst = color;
            dst += bpp;

//             if (j <= mirror_line) {
//                 // color = *(_palette + (*b1 + *b2 + _palette_index) % 256);
//                 color = *b1;
//             } else {
//                 // std::cout << (int) *(b1 - 2 * _w) << " ";
//                 b1 = (_buffer1 + _offset_y1 * _buffer_w + _offset_x1) + (j * 2 * pitch) + (i * bpp);
//                 // b2 = (_buffer2 + _offset_y2 * _buffer_w + _offset_x2) + (j * _buffer_w ) + (i * bpp);
//                 // color = *(_palette + (*b1 + *b2 + _palette_index) % 256);
//                 color = *b1;
//                 // color = *(_palette + (
//                 //     *(b1 + ((mirror_line - j) * _w)) + 
//                 //     *(b2 + ((mirror_line - j) * _w)) +
//                 //      _palette_index) % 256)
//                 // ;

// //                color = 0xFFFFFFFF - *(_palette + (*b1 + *b2 + _palette_index) % 256);
// //                 color = color | 0xFF404040;
//             }
//             *(Uint32 *)dst = color;
            
//             dst += bpp;
//             // b1++;
//             // b2++;
            
            // mirror_line = (_h / 2) + _h * sin((double) i / 47 ) * sin((double)_alpha * 4) * cos((double) _alpha * 3);
            mirror_line = (_h / 2) + 10 * sin((double) (i / 47.0) + _alpha * 2) * sin(_alpha);
        }
        // b1+=_w;
        // b2+=_w;
    }

    // if (mirror_line < _h / 2) {
    //     mirror_line += 2;
    // }
    
    SDL_UnlockSurface(surface);
}

