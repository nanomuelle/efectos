#include <iostream>
#include "SDL.h"

#define MAXRINGS 256
const float PI = 3.141592;

struct TRing {
	int numPuntos;
	int numStelas;
	float cx;
	float cy;
	float rx;
	float ry;
	float a;
	float ia;
	uint32_t color1;
	uint32_t color2;
};


class RingEffect {
    int _w;
    int _h;

    TRing *_rings;
    int _numRings; // = 1;

    float _cx; // = SCREEN_WIDTH / 2;
    float _cy; // = SCREEN_HEIGHT / 2;
    float _r; // = 150.0;

    float _ry; // = r;
    float _rx; // = r;
    float _a; // = 0;
    float _ia; // = PI / 180;

    float _numPuntos; // = 0;
    float _numStelas; // = 1;

    float _iia; //  = 0;

    void _renderRing(TRing * ring, SDL_Surface* surface);
    Uint8 _getR(Uint32 color) { return (color & 0x00FF0000) >> 16; }
    Uint8 _getG(Uint32 color) { return (color & 0x0000FF00) >> 8; }
    Uint8 _getB(Uint32 color) { return (color & 0x000000FF); }    

public:
    RingEffect(unsigned int w, unsigned int h) {
        _w = w;
        _h = h;
    }

    void init();
    void update(int deltaTime);
    void render(SDL_Surface* surface);
    void destroy() {
        delete[](_rings);
    };

};