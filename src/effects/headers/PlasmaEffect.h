#include <iostream>
#include "SDL.h"

class PlasmaEffect {
    unsigned int _w;
    unsigned int _h;
    unsigned int _buffer_w;
    unsigned int _buffer_h;
    unsigned char *_buffer1;
    unsigned char *_buffer2;
    uint32_t *_palette;
    unsigned int _palette_index;

    int _offset_y1;
    int _offset_x1;
    int _offset_y2;
    int _offset_x2;
    double _alpha;

public:
    PlasmaEffect(unsigned int w, unsigned int h) {
        _w = w;
        _h = h;
        _buffer_w = 2 * _w;
        _buffer_h = 2 * _h;
        _buffer1 = new unsigned char[_buffer_w * _buffer_h];
        _buffer2 = new unsigned char[_buffer_w * _buffer_h];
        _palette = new uint32_t[256];
        _palette_index = 0;

        _offset_y1 = _buffer_h / 2;
        _offset_x1 = _buffer_w / 2;
        _offset_y2 = _buffer_h / 2;
        _offset_x2 = _buffer_w / 2;

        _alpha = 0;
    };

    void init();
    void update(int deltaTime);
    void render(SDL_Surface* surface);
    void destroy() {
        delete[](_buffer1);
        delete[](_buffer2);
        delete[](_palette);
    };
};