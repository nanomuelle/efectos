#include <math.h>
#include "SDL.h"

#include "headers/RingEffect.h"

void RingEffect::init() {
	// allocate memory for all our stars
	_rings = new TRing[MAXRINGS];

	_numRings = 1;

	_numPuntos = 0;
	_numStelas = 1;

	_cx = _w / 2;
	_cy = _h / 2;

	_r = 150.0;
	_ry = _r;
	_rx = _r;

	_a = 0;
	_ia = PI / 90;
	_iia = 0;

	// generate some stars
	for (int i = 0; i < MAXRINGS; i++)
	{
		uint32_t color1 = 0xFF000000 
			| (i % 3 == 0 ? 0x00400000 : 0) 
			| (i % 3 == 1 ? 0x00004000 : 0) 
			| (i % 3 == 2 ? 0x00000040 : 0)
		;

		uint32_t color2 = 0xFF000000 
			| (i % 3 == 0 ? 0x00FF0000 : 0) 
			| (i % 3 == 1 ? 0x0000FF00 : 0) 
			| (i % 3 == 2 ? 0x000000FF : 0)
		;

		_rings[i].numPuntos = _numPuntos;
		_rings[i].numStelas = _numStelas;
		_rings[i].cx = _cx;
		_rings[i].cy = _cy;
		_rings[i].rx = _rx;
		_rings[i].ry = _ry;
		_rings[i].a = _a;
		_rings[i].ia = _ia;
		_rings[i].color1 = color1;
		_rings[i].color2 = color2;
	}
}

void RingEffect::update(int deltaTime) {
	_a += _ia;
	// iia = 0;

	// if (a < 4 * PI) {
	// 	numPuntos += 0.5;
	// } else if (a < 8 * PI) {
	// 	numStelas += 0.1;
	// } else if (a < 12 * PI) {
	// 	ry = r * cos(a);
	// } else if (a < 16 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// } else if (a < 20 * PI) {
	// 	numRings = 2;
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// } else if (a < 24 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	numStelas =  50;
	// } else if (a < 28 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	numPuntos -= 0.5;
	// 	iia += 0.01;
	// } else {
	// 	ry = r * cos(a);
	// 	numPuntos = 100;
	// 	numPuntos = 0;
	// 	numStelas = 1;
	// 	// a += 0.1;
	// 	r += 0.1;
	// }

	// if ( (int)(a * PI) % 360 * 40 == 0 && numRings < MAXRINGS) {
	// 	numRings += 1;
	// }

	
	if ( (int)(_a * 57.2958) % 15 == 0 && _numStelas < 20) {
		// std::cout << a << " " << a * PI << " " << (int)(a * PI) % 180 << "\n";
		_numStelas += 1;
	}

	if ( (int)(_a * 57.2958) % 180 == 0) {
		// std::cout << a << " " << a * PI << " " << (int)(a * PI) % 180 << "\n";
		_numRings += 1;
	}

	_ry = _r * cos(_a);
	_cx = (_w / 2) + _r * sin(_a / 2);
	
	for (int n = 0; n < _numRings; n++) {
		TRing *ring = &_rings[n];
		ring->numPuntos = _numPuntos;
		ring->numStelas = _numStelas; 
		ring->cx = _cx + 3 * n;
		ring->cy = _cy;
		ring->rx = n % 2 == 0 ? _ry : n % 3 == 0 ? _rx * sin(_a) : _rx;
		ring->ry = n % 2 != 0 ? _rx : n % 3 == 0 ? _rx : _ry;
		ring->a = _a;
	}
}

void RingEffect::render(SDL_Surface* surface) {    
    SDL_LockSurface(surface);
	for (int n = 0; n < _numRings; n++) {
//		TRing *ring = &rings[n];
		_renderRing(&_rings[n], surface);
	}    

	// if (a < 4 * PI) {
	// 	numPuntos += 0.5;
	// 	numPuntos = 0;
	// 	renderRing((int) numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFF400000);
	// } else if (a < 8 * PI) {
	// 	numStelas += 0.1;
	// 	renderRing((int) numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// 	// renderRing(100, 1, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFF400000);
	// } else if (a < 12 * PI) {
	// 	ry = r * cos(a);
	// 	// numPuntos -= 0.5;
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// } else if (a < 16 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// } else if (a < 20 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, ia, 0xFF004000, 0xFF00FF00);
	// 		// 	renderRing(cx, cy, ry, rx, a, ia * 3, 0xFF004040, 0xFF00FFff);
	// } else if (a < 48 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	numStelas =  25;
	// 	for (int k = 0; k < 3; ++k) {
	// 		renderRing(numPuntos, (int) numStelas, cx + k * 10, cy, rx, ry, a, ia, 0xFF400000 * ( 1 + k), 0xFFFF0000);
	// 		renderRing(numPuntos, (int) numStelas, cx, cy + k * 10, ry, rx, a, ia, 0xFF004000, 0xFF00FF00 *  (1 + k));
	// 		renderRing(numPuntos, (int) numStelas, cx - k * 10, cy, rx * sin(a), rx, a, ia, 0xFF000040 * (1 + k), 0xFF0000FF);
	// 		renderRing(numPuntos, (int) numStelas, cx, cy - k * 10, rx, rx * sin(a), a, ia, 0xFF404000, 0xFFFFFF00 * (1 + k));
	// 	}
	// 	// renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), ry * cos(a), a, ia + 4 * ia, 0xFF404040, 0xFFffffff);
	// } else if (a < 28 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	numPuntos -= 0.5;
	// 	iia += 0.01;
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, iia, 0xFF800000, 0xFF800000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, iia, 0xFF008000, 0xFF008000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), rx, a, iia, 0xFF000080, 0xFF000080);
	// } else {
	// 	ry = r * cos(a);
	// 	numPuntos = 100;
	// 	numPuntos = 0;
	// 	numStelas = 1;
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, iia, 0xFFFF0000, 0xFFFF0000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, iia, 0xFF00FF00, 0xFF00FF00);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), rx, a, iia, 0xFF0000FF, 0xFF0000FF);
	// 	a += 0.1;
	// 	r += 0.1;
	// }
    SDL_UnlockSurface(surface);
}

void RingEffect::_renderRing(TRing * ring, SDL_Surface* surface) {
	SDL_Rect point = {
		.x = 0,
		.y = 0,
		.w = 5,
		.h = 5
	};

	if (ring->numPuntos > 0) {
		float ik = PI / ring->numPuntos;
		for (float k = 0; k < 2 * PI; k += ik) {
			point.x = ring->cx + ring->rx * sin(k);
			point.y = ring->cy + ring->ry * cos(k);
			SDL_FillRect(surface, &point, ring->color1);
		}
	}

	int r = _getR(ring->color1);
	int g = _getG(ring->color1);
	int b = _getB(ring->color1);

	int ir = floor(((float) _getR(ring->color2) - r) / ring->numStelas);
	int ig = floor(((float) _getG(ring->color2) - g) / ring->numStelas);
	int ib = floor(((float) _getB(ring->color2) - b) / ring->numStelas);	

	int size = 3;
	point.h = size;
	point.w = size;
	for (int k = 0; k < ring->numStelas; k++) {
		point.x = ring->cx + ring->rx * sin(ring->a + k * ring->ia);
		point.y = ring->cy + ring->ry * cos(ring->a + k * ring->ia);
		point.h = 5 + size / ring->numStelas;
		point.w = 5 + size / ring->numStelas;
		SDL_FillRect(surface, &point, 0xFF000000 | r << 16 | g << 8 | b );
		r += ir;
		g += ig;
		b += ib;
	}
}
