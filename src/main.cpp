#include <iostream>
#include <math.h>

#include <SDL.h>

#include "./effects/headers/PlasmaEffect.h"
#include "./effects/headers/RingEffect.h"

// const float PI = 3.141592;

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//The window we'll be rendering to
SDL_Window* window = NULL;
//The surface contained by the window
SDL_Surface* screenSurface = NULL;

#define FPS 60
int lastTime = 0, currentTime, deltaTime;
float msFrame = 1 / (FPS / 1000.0f);

// Stars
// change this to adjust the number of stars
#define MAXSTARS 256

// this record contains the information for one star
struct TStar
{
	float x, y;             // position of the star
	unsigned char plane;    // remember which plane it belongs to
};

// this is a pointer to an array of stars
TStar *stars;


// Parametros para efecto ring
// struct TRing {
// 	int numPuntos;
// 	int numStelas;
// 	float cx;
// 	float cy;
// 	float rx;
// 	float ry;
// 	float a;
// 	float ia;
// 	uint32_t color1;
// 	uint32_t color2;
// };

// #define MAXRINGS 256

// TRing *rings;
// int numRings = 1;

// float cx = SCREEN_WIDTH / 2;
// float cy = SCREEN_HEIGHT / 2;
// float r = 150.0;

// float ry = r;
// float rx = r;
// float a = 0;
// float ia = PI / 180;

// float numPuntos = 0;
// float numStelas = 1;

// float iia = 0;


bool initSDL();
bool createWindow();
void adquireScreenSurface();

void update();
void render();

void close();
void waitTime();
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);

// stars
void initStars();
void updateStars();
void renderStars();
void printEvent(const SDL_Event * event);

// rings
void initRings();
void updateRings();
void renderRings();

// plasma
PlasmaEffect *plasma = new PlasmaEffect(SCREEN_WIDTH, SCREEN_HEIGHT);
RingEffect *ring = new RingEffect(SCREEN_WIDTH, SCREEN_HEIGHT);

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if (!initSDL())
	{
		std::cout << "Failed to initialize!\n";
		return 1;
	}
	else
	{
		plasma->init();
		ring->init();

		//Main loop flag
		bool quit = false;

		//Event handler
		SDL_Event e;

		//While application is running
		while (!quit)
		{
			//Handle events on queue
			while (SDL_PollEvent(&e) != 0)
			{
                if (e.type == SDL_WINDOWEVENT) {
                    printEvent(&e);
                    adquireScreenSurface();
                    // if (e.window.event == SDL_WINDOWEVENT_RESIZED) {
                    //     initSDL();
                    // }
                }

				if (e.type == SDL_KEYDOWN) {
					if (e.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
						quit = true;
					}
				}
				//User requests quit
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
			}

			// updates all
			update();

			//Render
			render();

			//Update the surface
			SDL_UpdateWindowSurface(window);
			waitTime();
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}

void printEvent(const SDL_Event * event)
{
    if (event->type == SDL_WINDOWEVENT) {
        switch (event->window.event) {
        case SDL_WINDOWEVENT_SHOWN:
            SDL_Log("Window %d shown", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_HIDDEN:
            SDL_Log("Window %d hidden", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_EXPOSED:
            SDL_Log("Window %d exposed", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_MOVED:
            SDL_Log("Window %d moved to %d,%d",
                    event->window.windowID, event->window.data1,
                    event->window.data2);
            break;
        case SDL_WINDOWEVENT_RESIZED:
            SDL_Log("Window %d resized to %dx%d",
                    event->window.windowID, event->window.data1,
                    event->window.data2);
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            SDL_Log("Window %d size changed to %dx%d",
                    event->window.windowID, event->window.data1,
                    event->window.data2);
            break;
        case SDL_WINDOWEVENT_MINIMIZED:
            SDL_Log("Window %d minimized", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_MAXIMIZED:
            SDL_Log("Window %d maximized", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_RESTORED:
            SDL_Log("Window %d restored", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_ENTER:
            SDL_Log("Mouse entered window %d",
                    event->window.windowID);
            break;
        case SDL_WINDOWEVENT_LEAVE:
            SDL_Log("Mouse left window %d", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_FOCUS_GAINED:
            SDL_Log("Window %d gained keyboard focus",
                    event->window.windowID);
            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            SDL_Log("Window %d lost keyboard focus",
                    event->window.windowID);
            break;
        case SDL_WINDOWEVENT_CLOSE:
            SDL_Log("Window %d closed", event->window.windowID);
            break;
#if SDL_VERSION_ATLEAST(2, 0, 5)
        case SDL_WINDOWEVENT_TAKE_FOCUS:
            SDL_Log("Window %d is offered a focus", event->window.windowID);
            break;
        case SDL_WINDOWEVENT_HIT_TEST:
            SDL_Log("Window %d has a special hit test", event->window.windowID);
            break;
#endif
        default:
            SDL_Log("Window %d got unknown event %d",
                    event->window.windowID, event->window.event);
            break;
        }
    }
}

void adquireScreenSurface() {
    screenSurface = SDL_GetWindowSurface(window);
}

bool createWindow() {
	//Create window
	// window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	window = SDL_CreateWindow(
        "SDL Tutorial", 
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        SCREEN_WIDTH, SCREEN_HEIGHT, 
        SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);

	if (window == NULL)
	{
		std::cout << "Window could not be created! SDL_Error: %s\n" << SDL_GetError();
		return false;
	}
	//Get window surface
	adquireScreenSurface();
    return true;
}

bool initSDL() {

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not initialize! SDL_Error: %s\n" << SDL_GetError();
		return false;
	}    
	return createWindow();
}

void update(){
	plasma->update(deltaTime);
	ring->update(deltaTime);
}

void render() {
	//Fill with black
	SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0, 0, 0));

	plasma->render(screenSurface);
	ring->render(screenSurface);

	// if (a < 4 * PI) {
	// 	numPuntos += 0.5;
	// 	numPuntos = 0;
	// 	renderRing((int) numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFF400000);
	// } else if (a < 8 * PI) {
	// 	numStelas += 0.1;
	// 	renderRing((int) numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// 	// renderRing(100, 1, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFF400000);
	// } else if (a < 12 * PI) {
	// 	ry = r * cos(a);
	// 	// numPuntos -= 0.5;
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// } else if (a < 16 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// } else if (a < 20 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, ia, 0xFF004000, 0xFF00FF00);
	// 		// 	renderRing(cx, cy, ry, rx, a, ia * 3, 0xFF004040, 0xFF00FFff);
	// } else if (a < 48 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	numStelas =  25;
	// 	for (int k = 0; k < 3; ++k) {
	// 		renderRing(numPuntos, (int) numStelas, cx + k * 10, cy, rx, ry, a, ia, 0xFF400000 * ( 1 + k), 0xFFFF0000);
	// 		renderRing(numPuntos, (int) numStelas, cx, cy + k * 10, ry, rx, a, ia, 0xFF004000, 0xFF00FF00 *  (1 + k));
	// 		renderRing(numPuntos, (int) numStelas, cx - k * 10, cy, rx * sin(a), rx, a, ia, 0xFF000040 * (1 + k), 0xFF0000FF);
	// 		renderRing(numPuntos, (int) numStelas, cx, cy - k * 10, rx, rx * sin(a), a, ia, 0xFF404000, 0xFFFFFF00 * (1 + k));
	// 	}
	// 	// renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), ry * cos(a), a, ia + 4 * ia, 0xFF404040, 0xFFffffff);
	// } else if (a < 28 * PI) {
	// 	ry = r * cos(a);
	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// 	numPuntos -= 0.5;
	// 	iia += 0.01;
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, iia, 0xFF800000, 0xFF800000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, iia, 0xFF008000, 0xFF008000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), rx, a, iia, 0xFF000080, 0xFF000080);
	// } else {
	// 	ry = r * cos(a);
	// 	numPuntos = 100;
	// 	numPuntos = 0;
	// 	numStelas = 1;
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, iia, 0xFFFF0000, 0xFFFF0000);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, iia, 0xFF00FF00, 0xFF00FF00);
	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), rx, a, iia, 0xFF0000FF, 0xFF0000FF);
	// 	a += 0.1;
	// 	r += 0.1;
	// }
		
	// // solo circulo con cada vez mas puntos
	// // renderRing(0, 50, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFFFF0000);

	// // // giro sobre eje y
	// // if (a > 2 * PI) {
	// // 	ry = r * cos(a);
	// // }
	
	// // // desplazamiento horizontal
	// // if (a > 4 * PI) {
	// // 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	// // }

	// // renderRing(cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFFFF0000);

	// // if (a > 6 * PI) {
	// // 	renderRing(cx, cy, ry, rx, a, ia * 3, 0xFF004040, 0xFF00FFff);
	// // }

	// // if (a > 8 * PI) {
	// // 	renderRing(cx, cy, ry * sin(a), rx * cos(a), a, ia + 8 * ia, 0xFF400040,  0xFFFF00FF);
	// // 	renderRing(cx, cy, ry * cos(a), rx * sin(a), a, ia + 8 * ia, 0xFF404040, 0xFFffffff);
	// // }

}

void close() {
	// free memory
	delete[](stars);

	// delete[](rings);
	plasma->destroy();
	ring->destroy();

	//Destroy window
	SDL_DestroyWindow(window);
	//Quit SDL subsystems
	SDL_Quit();
}

void waitTime() {
	currentTime = SDL_GetTicks();
	deltaTime = currentTime - lastTime;
	if (deltaTime < (int)msFrame) {
		SDL_Delay((int)msFrame - deltaTime);
	}
	lastTime = currentTime;

}

/*
* Set the pixel at (x, y) to the given value
* NOTE: The surface must be locked before calling this!
*/
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	// Clipping
	if ((x < 0) || (x >= SCREEN_WIDTH) || (y < 0) || (y >= SCREEN_HEIGHT)) 
		return;

	int bpp = surface->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to set */
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch (bpp) {
	case 1:
		*p = pixel;
		break;

	case 2:
		*(Uint16 *)p = pixel;
		break;

	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
			p[0] = (pixel >> 16) & 0xff;
			p[1] = (pixel >> 8) & 0xff;
			p[2] = pixel & 0xff;
		}
		else {
			p[0] = pixel & 0xff;
			p[1] = (pixel >> 8) & 0xff;
			p[2] = (pixel >> 16) & 0xff;
		}
		break;

	case 4:
		*(Uint32 *)p = pixel;
		break;
	}
}

void initStars() {
	// allocate memory for all our stars
	stars = new TStar[MAXSTARS];
	// randomly generate some stars
	for (int i = 0; i<MAXSTARS; i++)
	{
		stars[i].x = (float)(rand() % SCREEN_WIDTH);
		stars[i].y = (float)(rand() % SCREEN_HEIGHT);
		stars[i].plane = rand() % 3;     // star colour between 0 and 2
	}

}

void updateStars() {
	// update all stars
	for (int i = 0; i < MAXSTARS; i++)
	{
		// move this star right, determine how fast depending on which
		// plane it belongs to
		stars[i].x += (deltaTime + (float)stars[i].plane) * 0.15f;
		// check if it's gone out of the right of the screen
		if (stars[i].x > SCREEN_WIDTH)
		{
			// if so, make it return to the left
			stars[i].x = -(float)(rand() % SCREEN_WIDTH);
			// and randomly change the y position
			stars[i].y = (float)(rand() % SCREEN_HEIGHT);
		}
	}
}

void renderStars() {
	// update all stars
	for (int i = 0; i<MAXSTARS; i++)
	{
		// draw this star, with a colour depending on the plane
		unsigned int color = 0;
		switch (1+stars[i].plane) {
		case 1:
			color = 0xFF606060; // dark grey
			break;
		case 2:
			color = 0xFFC2C2C2; // light grey
			break;
		case 3:
			color = 0xFFFFFFFF; // white
			break;
		}

        SDL_Rect r0 = {
            .x = (int)stars[i].x - 2 * stars[i].plane, 
            .y = (int)stars[i].y - 2 * stars[i].plane,
            .w = 1 + 4 * stars[i].plane,
            .h = 1 + 4 * stars[i].plane
        };
        // SDL_FillRect(screenSurface, &r1, SDL_MapRGB(screenSurface->format, 0, 0, 0));
        SDL_FillRect(screenSurface, &r0, (int) 0x00808080);

		// putpixel(screenSurface, (int)stars[i].x, (int)stars[i].y, color);        
        SDL_Rect r1 = {
            .x = (int)stars[i].x - 2 * stars[i].plane, 
            .y = (int)stars[i].y,
            .w = 1 + 4 * stars[i].plane,
            .h = 1
        };
        // SDL_FillRect(screenSurface, &r1, SDL_MapRGB(screenSurface->format, 0, 0, 0));
        SDL_FillRect(screenSurface, &r1, color);

        SDL_Rect r2 = {
            .x = (int)stars[i].x, 
            .y = (int)stars[i].y - 2 * stars[i].plane,
            .w = 1,
            .h = 1 + 4 * stars[i].plane
        };
        SDL_FillRect(screenSurface, &r2, color);
	}
}

/** rings **/
// void initRings() {
// 	// allocate memory for all our stars
// 	rings = new TRing[MAXRINGS];

// 	numRings = 1;

// 	numPuntos = 0;
// 	numStelas = 1;

// 	cx = SCREEN_WIDTH / 2;
// 	cy = SCREEN_HEIGHT / 2;

// 	r = 150.0;
// 	ry = r;
// 	rx = r;

// 	a = 0;
// 	ia = PI / 90;
// 	iia = 0;

// 	// generate some stars
// 	for (int i = 0; i<MAXRINGS; i++)
// 	{
// 		uint32_t color1 = 0xFF000000 
// 			| (i % 3 == 0 ? 0x00400000 : 0) 
// 			| (i % 3 == 1 ? 0x00004000 : 0) 
// 			| (i % 3 == 2 ? 0x00000040 : 0)
// 		;

// 		uint32_t color2 = 0xFF000000 
// 			| (i % 3 == 0 ? 0x00FF0000 : 0) 
// 			| (i % 3 == 1 ? 0x0000FF00 : 0) 
// 			| (i % 3 == 2 ? 0x000000FF : 0)
// 		;

// 		rings[i].numPuntos = numPuntos;
// 		rings[i].numStelas = numStelas;
// 		rings[i].cx = cx;
// 		rings[i].cy = cy;
// 		rings[i].rx = rx;
// 		rings[i].ry = ry;
// 		rings[i].a = a;
// 		rings[i].ia = ia;
// 		rings[i].color1 = color1;
// 		rings[i].color2 = color2;
// 	}
// }

// void updateRings() {
// 	a += ia;
// 	// iia = 0;

// 	// if (a < 4 * PI) {
// 	// 	numPuntos += 0.5;
// 	// } else if (a < 8 * PI) {
// 	// 	numStelas += 0.1;
// 	// } else if (a < 12 * PI) {
// 	// 	ry = r * cos(a);
// 	// } else if (a < 16 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// } else if (a < 20 * PI) {
// 	// 	numRings = 2;
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// } else if (a < 24 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// 	numStelas =  50;
// 	// } else if (a < 28 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// 	numPuntos -= 0.5;
// 	// 	iia += 0.01;
// 	// } else {
// 	// 	ry = r * cos(a);
// 	// 	numPuntos = 100;
// 	// 	numPuntos = 0;
// 	// 	numStelas = 1;
// 	// 	// a += 0.1;
// 	// 	r += 0.1;
// 	// }

// 	// if ( (int)(a * PI) % 360 * 40 == 0 && numRings < MAXRINGS) {
// 	// 	numRings += 1;
// 	// }

	
// 	if ( (int)(a * 57.2958) % 15 == 0 && numStelas < 20) {
// 		// std::cout << a << " " << a * PI << " " << (int)(a * PI) % 180 << "\n";
// 		numStelas += 1;
// 	}

// 	if ( (int)(a * 57.2958) % 180 == 0) {
// 		// std::cout << a << " " << a * PI << " " << (int)(a * PI) % 180 << "\n";
// 		numRings += 1;
// 	}

// 	ry = r * cos(a);
// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
	
// 	for (int n = 0; n < numRings; n++) {
// 		TRing *ring = &rings[n];
// 		ring->numPuntos = numPuntos;
// 		ring->numStelas = numStelas; 
// 		ring->cx = cx + 3 * n;
// 		ring->cy = cy;
// 		ring->rx = n % 2 == 0 ? ry : n % 3 == 0 ? rx * sin(a) : rx;
// 		ring->ry = n % 2 != 0 ? rx : n % 3 == 0 ? rx : ry;
// 		ring->a = a;
// 	}
// }

// void renderRings() {
// 	for (int n = 0; n < numRings; n++) {
// //		TRing *ring = &rings[n];
// 		renderRing(&rings[n]);
// 	}

// 	// if (a < 4 * PI) {
// 	// 	numPuntos += 0.5;
// 	// 	numPuntos = 0;
// 	// 	renderRing((int) numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFF400000);
// 	// } else if (a < 8 * PI) {
// 	// 	numStelas += 0.1;
// 	// 	renderRing((int) numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
// 	// 	// renderRing(100, 1, cx, cy, rx, ry, a, ia * 2, 0xFF400000, 0xFF400000);
// 	// } else if (a < 12 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	// numPuntos -= 0.5;
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
// 	// } else if (a < 16 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
// 	// } else if (a < 20 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, ia, 0xFF400000, 0xFFFF0000);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, ia, 0xFF004000, 0xFF00FF00);
// 	// 		// 	renderRing(cx, cy, ry, rx, a, ia * 3, 0xFF004040, 0xFF00FFff);
// 	// } else if (a < 48 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// 	numStelas =  25;
// 	// 	for (int k = 0; k < 3; ++k) {
// 	// 		renderRing(numPuntos, (int) numStelas, cx + k * 10, cy, rx, ry, a, ia, 0xFF400000 * ( 1 + k), 0xFFFF0000);
// 	// 		renderRing(numPuntos, (int) numStelas, cx, cy + k * 10, ry, rx, a, ia, 0xFF004000, 0xFF00FF00 *  (1 + k));
// 	// 		renderRing(numPuntos, (int) numStelas, cx - k * 10, cy, rx * sin(a), rx, a, ia, 0xFF000040 * (1 + k), 0xFF0000FF);
// 	// 		renderRing(numPuntos, (int) numStelas, cx, cy - k * 10, rx, rx * sin(a), a, ia, 0xFF404000, 0xFFFFFF00 * (1 + k));
// 	// 	}
// 	// 	// renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), ry * cos(a), a, ia + 4 * ia, 0xFF404040, 0xFFffffff);
// 	// } else if (a < 28 * PI) {
// 	// 	ry = r * cos(a);
// 	// 	cx = (SCREEN_WIDTH / 2) + r * sin(a / 2);
// 	// 	numPuntos -= 0.5;
// 	// 	iia += 0.01;
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, iia, 0xFF800000, 0xFF800000);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, iia, 0xFF008000, 0xFF008000);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), rx, a, iia, 0xFF000080, 0xFF000080);
// 	// } else {
// 	// 	ry = r * cos(a);
// 	// 	numPuntos = 100;
// 	// 	numPuntos = 0;
// 	// 	numStelas = 1;
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx, ry, a, iia, 0xFFFF0000, 0xFFFF0000);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, ry, rx, a, iia, 0xFF00FF00, 0xFF00FF00);
// 	// 	renderRing(numPuntos, (int) numStelas, cx, cy, rx * sin(a), rx, a, iia, 0xFF0000FF, 0xFF0000FF);
// 	// 	a += 0.1;
// 	// 	r += 0.1;
// 	// }
// }

// void renderRing(TRing * ring) {
// 	SDL_Rect point = {
// 		.x = 0,
// 		.y = 0,
// 		.w = 5,
// 		.h = 5
// 	};

// 	if (ring->numPuntos > 0) {
// 		float ik = PI / ring->numPuntos;
// 		for (float k = 0; k < 2 * PI; k += ik) {
// 			point.x = ring->cx + ring->rx * sin(k);
// 			point.y = ring->cy + ring->ry * cos(k);
// 			SDL_FillRect(screenSurface, &point, ring->color1);
// 		}
// 	}

// 	int r = getR(ring->color1);
// 	int g = getG(ring->color1);
// 	int b = getB(ring->color1);

// 	int ir = floor(((float) getR(ring->color2) - r) / ring->numStelas);
// 	int ig = floor(((float) getG(ring->color2) - g) / ring->numStelas);
// 	int ib = floor(((float) getB(ring->color2) - b) / ring->numStelas);	

// 	int size = 3;
// 	point.h = size;
// 	point.w = size;
// 	for (int k = 0; k < ring->numStelas; k++) {
// 		point.x = ring->cx + ring->rx * sin(ring->a + k * ring->ia);
// 		point.y = ring->cy + ring->ry * cos(ring->a + k * ring->ia);
// 		point.h = 5 + size / ring->numStelas;
// 		point.w = 5 + size / ring->numStelas;
// 		SDL_FillRect(screenSurface, &point, 0xFF000000 | r << 16 | g << 8 | b );
// 		r += ir;
// 		g += ig;
// 		b += ib;
// 	}
// }

