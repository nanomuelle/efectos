hello.out: src/main.cpp
	# clang++ main.cpp --include-directory=../SDL2.framework/Headers --library-directory=<arg> -o bin/hello 
	clang++ \
		-g src/main.cpp \
		-g src/effects/*.cpp \
		-o bin/demo \
		-I /Library/Frameworks/SDL2.framework/Headers \
		-I /src/effects/headers \
		-F /Library/Frameworks \
		-framework SDL2

# -std=c++11 \
# -stdlib=libc++ \
# -F/Library/Frameworks \
# -framework SDL2 \
# --include-directory=/Library/Frameworks/SDL2.framework/Headers \